# Define the kmod package name here.
%define kmod_name ivtv
%define kmod_version 1.4.3
%define kmod_release 1%{?dist}

Name:    %{kmod_name}-kmod
Version: %{kmod_version}
Release: %{kmod_release}
Group:   System Environment/Kernel
License: GPLv2
Summary: ivtv driver module for multi-standard PVR-150
URL:     http://www.kernel.org

BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-build-%(%{__id_u} -n)
BuildRequires: redhat-rpm-config
ExclusiveArch: i686 x86_64

# Sources.
Source0:  %{kmod_name}-%{version}.tar.gz
Source10: kmodtool-%{kmod_name}

# If kversion isn't defined on the rpmbuild line, build for the current kernel.
%{!?kversion: %define kversion %(uname -r)}

# Magic hidden here.
%define kmodtool sh %{SOURCE10}
%{expand:%(%{kmodtool} rpmtemplate %{kmod_name} %{kversion} "")}

%description
This package contains the ivtv Linux driver.
It is built to depend upon the specific ABI provided by a range of releases
of the same variant of the Linux kernel and not on any one specific build.

%prep
%setup -q -n %{kmod_name}-%{kmod_version}
mv -f src/* ./
rm -rf src

%build
ksrc=%{_usrsrc}/kernels/%{kversion}
%{__make} -C "${ksrc}" %{?_smp_mflags} modules M=$PWD KSRC=${ksrc}
cat << EOF >kmod-%{kmod_name}.conf
override %{kmod_name} * weak-updates/%{kmod_name}
EOF

%install
export INSTALL_MOD_PATH=%{buildroot}
export INSTALL_MOD_DIR=extra/%{kmod_name}
ksrc=%{_usrsrc}/kernels/%{kversion}

%{__make} -C "${ksrc}" %{?_smp_mflags} modules_install M=$PWD
%{__rm} -f %{buildroot}/lib/modules/%{kversion}/modules.*

%{__install} -d %{buildroot}%{_sysconfdir}/depmod.d
%{__install} -m644 kmod-%{kmod_name}.conf \
    %{buildroot}%{_sysconfdir}/depmod.d/

# Set the module(s) to be executable, so that they will be stripped when packaged.
find %{buildroot} -type f -name \*.ko -exec %{__chmod} u+x \{\} \;

%clean
%{__rm} -rf %{buildroot}

%changelog
* Wed Oct 14 2015 Artem Kharitonov <artem@sysert.ru> - 1.4.3-1
- Initial build.
