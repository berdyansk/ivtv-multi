ivtv driver for PAL/SECAM Hauppauge PVR-150
========

This is the ivtv module as seen in the EL7 kernel specifically updated
to support multi standard PVR-150 cards. With stock driver, the only way
to set the standard is by passing 'pal' or 'secam' option to modprobe;
this patched version exposes both standards to userspace tools.
